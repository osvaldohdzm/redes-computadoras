# Analizador LLC

Aunque el protocolo UDP no está orientado a la conexión, es posible realizar un escaneo. No tiene un paquete SYN como el protocolo TCP, sin embargo si un paquete se envía a un puerto que no está abierto, responde con un mensaje ICMP Port Unreachable. La mayoría de los escáners de puertos UDP usan este método, e infieren que si no hay respuesta, el puerto está abierto. Pero en el caso que esté filtrado por un firewall, este método dará una información errónea. Para este programa  [ver](http://www.arubanetworks.com/techdocs/ArubaOS_63_Web_Help/Content/ArubaFrameStyles/Defaults/Default_Open_Ports.htm) y [ver](http://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-sg-es-4/ch-ports.html).

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux. De forma opcional se puede instalar el escaner nmap para corroborar el escaneo del programa al implementar desarrollos adicionales.

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o udp.out udp.c -lpthread -w

```

La razón del parametro "-w" es para omitir warnnings remanentes. O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile udp

```

### Ejecución

Para ejecutar:

```
sudo ./[programa] [host_objetivo]  -s 65 -f 70
```

Se ha elegido en este caso el puerto 68 por ser usado para servicio de DHCPC.

### Ejemplo

```
sudo ./udp.out localhost -s 64 -f 70
```

### Comentarios

Instalar NMAP:

```
apt-get install nmap
```

Escaneo básico de puertos:
```
nmap 192.168.1.100
```

Buscar puertos UDP de una máquina:
```
sudo nmap -sU 192.168.1.100
```
Buscar puertos UDP de una máquina:
```
nmap -sT 192.168.1.100
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Juan Jesus Alcaraz** - *Profesor*
* **Osvaldo Hernández Morales** - *Estudiante*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
