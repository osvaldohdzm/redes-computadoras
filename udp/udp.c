/**
 * Autor: Osvaldo Hernández Morales
 * Fecha: 31/01/2017, 25/04/2017
 */

#include <signal.h>             /** Cabecera estandar para manejo de señales. */
#include <stdio.h>              /** Cabecera estandar de entrada y salida. */
#include <stdlib.h> 		        /** Cabecera estandar con funciones para gestión de memoria dinámica, control de procesos y otras. */
#include <unistd.h>             /** Cabecera para acceso a API sistema operativo POSIX para versiones UNIX. */
#include <string.h>             /** Cabecera estandar con funciones y tipos para manipulación de memoria. */
#include <sys/types.h>          /** Cabecera con funciones de búsqueda y ordenamiento de archivos*/
#include <sys/socket.h>         /** Cabecera para el manejo de sockets UNIX. */
#include <netinet/in_systm.h>   /** Funciones para manejo de puertos a bajo nivel en peticiones ICMP e IP con sockets crudos. */
#include <netinet/in.h>         /** Estructuras y macros para direcciones de internet. */
#include <netinet/ip.h>         /** Implementacion del protocolo TPC/IP para linux. */
#include <netinet/udp.h>        /** Cabecera con estructuras para protocolo UDP */
#include <netinet/ip_icmp.h>    /** Cabecera con estructuras para protocolo ICMP */
#include <netdb.h>   		        /** Definiciones de operaciones de base de datos de red. */
#include <pthread.h>            /** Cabecera UNIX para gestión de subprocesos. */
#include <errno.h>              /** Para la salida a errno - el número de error. */
#include <arpa/inet.h>          /** Cabecera para usar funciones con dircciones ip. */
#include <getopt.h>             /** Cabecera auxiliar para el manejo de argumentos en linea de comandos en sistemas POSIX */

#define PORT_UNKNOWN 0
#define PORT_CLOSED 1
#define PORT_OPEN 2

struct scaninfo {
 unsigned short start;
 unsigned short end;
 char hostname[256];
 struct in_addr addr;
 char data[128];
 useconds_t interval;
 unsigned int attempts;
 unsigned char *status;
 unsigned int wait;
};

#define MAX_PAYLOAD 128
#define MAX_HOSTNAME 256


#define BUFSIZE 4096
void enviado(struct scaninfo *);
void *receiver(void *);


void parse_scanpara(int argc, char *argv[], struct scaninfo *info);


static struct scaninfo info;


static void sig_int(int);

static void MostrarResultado(void);

int main(int argc, char *argv[])
{
 pthread_t rid;


 parse_scanpara(argc, argv, &info);

 if (signal(SIGINT, sig_int) < 0) {
  perror("señal error");
  exit(-1);
 }

 if (pthread_create(&rid, NULL, receiver, &info) != 0) {
  perror("pthread_create error");
  exit(-1);
 }
 if (pthread_detach(rid) != 0) {
  perror("pthread_detach error");
  exit(-1);
 }


 enviado(&info);

 MostrarResultado();

 return 0;
}


static void MostrarResultado(void)
{
 unsigned short total;
 unsigned short i, j;

 total = info.end - info.start + 1;


 printf("\npuerto abierto:\t");
 for (i = 0, j = 0; i < total; i++) {
  if (info.status[i] == PORT_OPEN) {
   j++;
   printf("%d ", i + info.start);
   if (j == 10) {
    printf("\n\t\t");
    j = 0;
   }
  }
 }


 printf("\npuerto cerrado:\t");
 for (i = 0, j = 0; i < total; i++) {
  if (info.status[i] == PORT_CLOSED) {
   j++;
   printf("%d ", i + info.start);
   if (j == 10) {
    printf("\n\t\t");
    j = 0;
   }
  }
 }


 printf("\npuerto abierto (filtrado):\t");
 for (i = 0, j = 0; i < total; i++) {
  if (info.status[i] == PORT_UNKNOWN) {
   j++;
   printf("%d ", i + info.start);
   if (j == 10) {
    printf("\n\t\t");
    j = 0;
   }
  }
 }
 printf("\n");
}

static void sig_int(int signo)
{
 MostrarResultado();
 exit(0);
}


static void alarmHanlder(int signo)
{
 alarm(1);
}


void enviado(struct scaninfo *arg)
{
 if (arg == NULL)
  return;

 int sockfd;
 unsigned short i, start, end;
 int len, total;
 struct sockaddr_in servaddr;
 struct scaninfo *info = (struct scaninfo *) arg;

 start = info->start;
 end = info->end;

 if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
  perror("socket(AF_INET,SOCK_DGRAM,0) error");
  exit(-1);
 }
 memset(&servaddr, 0, sizeof(servaddr));
 servaddr.sin_family = AF_INET;
 memcpy(&servaddr.sin_addr, &info->addr, sizeof(servaddr.sin_addr));
 len = strlen(info->data);

 setbuf(stdout, NULL);
 for (total = 1; total <= info->attempts; total++) {
  printf("enviando UDP paquetes(%d)", total);
  for (i = start; i <= end; i++) {
   servaddr.sin_port = htons(i);
   if (sendto(sockfd, info->data, len, 0,
        (struct sockaddr *) &servaddr,
        sizeof(servaddr)) < 0) {
    perror("error de envio");
    exit(-1);
   }
   usleep(info->interval);
   printf(".");
  }
  printf("\ncomplete enviando(%d)\n", total);
  if (total == info->attempts)
   break;
  for (i = 1; i <= 20; i++) {
   printf(".");
   usleep(100000);
  }
  printf("\n");
 }

 printf("+++++++++++++++++++++++++++++\nTodos los paquetes han sido enviados!\n");
 printf("esperar %d segundos para salir!\n", info->wait);


 struct sigaction act;
 act.sa_handler = alarmHanlder;
 sigemptyset(&act.sa_mask);
 act.sa_flags = SA_INTERRUPT;
 sigaction(SIGALRM, &act, NULL);
 alarm(info->wait);

 char buf[1024];
 struct sockaddr_in recvaddr;
 socklen_t recvlen;
 ssize_t n;
 while ((n = recvfrom(sockfd, buf, 1024, 0,
       (struct sockaddr *) &recvaddr, &recvlen)) > 0) {
  if (recvaddr.sin_addr.s_addr == servaddr.sin_addr.s_addr) {
   unsigned short port = ntohs(recvaddr.sin_port);
   info->status[port - info->start] = PORT_OPEN;
  }
 }

 close(sockfd);

 return;
}

#define ICMP_LEN 8

void *receiver(void *arg)
{
 int sockfd;
 struct scaninfo *sip;
 struct sockaddr_in raddr;
 socklen_t len;
 char buf[BUFSIZE];
 struct iphdr *ip;
 unsigned int iplen;
 struct icmphdr *icmp;
 struct udphdr *udp;
 char straddr[256];
 unsigned short start, end, total;
 unsigned short port;

 sip = (struct scaninfo *) arg;
 if (sip == NULL)
  return NULL;

 start = sip->start;
 end = sip->end;
 total = end - start + 1;

 if ((sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0) {
  perror("socket(AF_INET,SOCK_RAW,IPPROTO_ICMP) error");
  exit(-1);
 }
 setuid(getuid());

 memset(&raddr, 0, sizeof(raddr));
 len = sizeof(raddr);
 raddr.sin_family = AF_INET;
 memcpy(&raddr.sin_addr, &sip->addr, sizeof(raddr.sin_addr));





 if (connect(sockfd, (struct sockaddr *) &raddr, len) < 0) {
  perror("connect error");
  exit(-1);
 }


 while (recv(sockfd, buf, BUFSIZE, 0) > 0) {
  ip = (struct iphdr *) buf;

  if (inet_ntop(AF_INET, &ip->saddr, straddr, 256) != 1)
   strncpy(straddr, "desconocido", 256);

  if (ip->protocol == IPPROTO_ICMP) {
   iplen = ip->ihl << 2;
   icmp = (struct icmphdr *) (buf + iplen);
   if (icmp->type != ICMP_DEST_UNREACH || icmp->code != ICMP_PORT_UNREACH) {
    printf
     ("paquete inesperado de %s, tipo = %d, codigo = %d\n",
      straddr, icmp->type, icmp->code);
    continue;
   }

   ip = (struct iphdr *) (buf + iplen + ICMP_LEN);
   iplen = ip->ihl << 2;
   udp = (struct udphdr *) ((char *) ip + iplen);
   port = ntohs(udp->dest);
   if (port < start || port > end) {
    printf("paquete inesperado de %s, puerto udp = %d\n",
        straddr, port);
    continue;
   }
   sip->status[port - start] = PORT_CLOSED;
  }
 }


 close(sockfd);
 return NULL;
}



static void help(void)
{
 printf("Uso:udp_scan\tdest\t[-s puerto_inicial]\n");
 printf("\t\t\t[-f puero_final]\n");
 printf("\t\t\t[-i intervalo]\n");
 printf("\t\t\t[-a intentos]\n");
 printf("\t\t\t[-p payload]\n");
 printf("\t\t\t[-w tiempo_para_salir]\n");
 printf("\t\t\t[-h ayuda]\n");
 exit(0);
}

void parse_scanpara(int argc, char *argv[], struct scaninfo *info)
{
 if (info == NULL)
  return;
 memset(info, 0, sizeof(struct scaninfo));

 info->attempts = 1;
 info->interval = 500000;

 strncpy(info->data, "hello world!", MAX_PAYLOAD);

 int opt;
 struct option longopts[] = {
  {"ayuda", 0, NULL, 'h'},
  {"puerto-inicial", 1, NULL, 's'},
  {"puerto-final", 1, NULL, 'f'},
  {"intervalo", 1, NULL, 'i'},
  {"intentos", 1, NULL, 'a'},
  {"payload", 1, NULL, 'p'},
  {"espera", 1, NULL, 'w'},
  {0, 0, 0, 0}
 };

 while ((opt =
   getopt_long(argc, argv, "hs:f:i:a:p:w:", longopts,
      NULL)) != -1) {
  switch (opt) {
  case 'h':
   help();
   break;
  case 's':
   info->start = atoi(optarg);
   break;
  case 'f':
   info->end = atoi(optarg);
   break;
  case 'i':
   info->interval = atoi(optarg) * 1000;
   break;
  case 'a':
   info->attempts = atoi(optarg);
   break;
  case 'p':
   strncpy(info->data, optarg, MAX_PAYLOAD);
   break;
  case 'w':
   info->wait = atoi(optarg);
   break;
  default:
   help();
   break;
  }
 }
 if (optind != argc - 1)
  help();

 if (info->start == 0 || info->end == 0 || info->end < info->start
  || info->attempts <= 0) {
  fprintf(stderr, "Numero de puerto invalido o intentos\n");
  exit(-1);
 }

 if (info->wait == 0) {
  info->wait =
   info->interval / 1000000.0 * (info->end - info->start + 1);
  if (info->wait == 0) {
   info->wait = 1;
  }
 }
 info->status =
  (unsigned char *) malloc((info->end - info->start + 1) *
         sizeof(unsigned char));
 if (info->status == NULL) {
  perror("malloc error");
  exit(-1);
 }
 memset(info->status, 0, info->end - info->start + 1);

 if (inet_pton(AF_INET, argv[optind], &info->addr) != 1) {
  struct hostent *hp;
  if ((hp = gethostbyname(argv[optind])) == NULL) {
   fprintf(stderr, "Destino invalido - %s\n", argv[optind]);
   exit(-1);
  }
  strncpy(info->hostname, argv[optind], MAX_HOSTNAME);
  memcpy(&info->addr, hp->h_addr, sizeof(info->addr));
 }
}
