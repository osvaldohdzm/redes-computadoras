# Ping

Es una utilidad diagnóstica1 en redes de computadoras que comprueba el estado de la comunicación del host local con uno o varios equipos remotos de una red IP por medio del envío de paquetes ICMP de solicitud (ICMP Echo Request) y de respuesta (ICMP Echo Reply).

Para más información de las funciones usadas en este programa se sugiere usar:
```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux.

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o ping.out ping.c

```

O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile ping

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa] [hostname o ip]
```

### Ejemplo

```
 sudo ./ping.out google.com
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
