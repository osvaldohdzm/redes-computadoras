# Escaner de puertos TCP

El término escáner de puertos o escaneo de puertos se emplea para designar la acción de analizar por medio de un programa el estado de los puertos de una máquina conectada a una red de comunicaciones. Detecta si un puerto está abierto, cerrado, o protegido por un cortafuegos. Este programa solo muestra los puertos que estan abiertos.

- Si al enviar un paquete SYN a un puerto específico, el destino devuelve un SYN/ACK, el puerto está abierto y escuchando conexiones.
- En otro caso, si regresa un paquete RST, el puerto está cerrado.
- Por último, si no regresa el paquete, o si se recibe un paquete ICMP Port Unreachable, el puerto está filtrado por algún tipo de cortafuegos.

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux.

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o tcp.out tcp.c -lpthread

```

O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile tcp

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa]
```

### Ejemplo

```
 sudo ./llc.out
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
