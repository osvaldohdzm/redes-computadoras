/**
 * Autor: Osvaldo Hernández Morales
 * Fecha: 31/01/2017, 25/04/2017
 */

#include <stdio.h>          /** Cabecera estandar de entrada y salida. */
#include <string.h>         /** Cabecera estandar con funciones y tipos para manipulación de memoria. */
#include <stdlib.h> 				/** Cabecera estandar con funciones para gestión de memoria dinámica, control de procesos y otras. */
#include <sys/socket.h>     /** Cabecera para el manejo de sockets UNIX. */
#include <errno.h>          /** Para la salida a errno - el número de error. */
#include <pthread.h>        /** Cabecera UNIX para gestión de subprocesos. */
#include <netdb.h>          /** Para la función hostend. */
#include <arpa/inet.h>      /** Cabecera para usar funciones con dircciones ip. */
#include <netinet/tcp.h>    /** Implementacion del protocolo TCP para linux. */
#include <netinet/ip.h>     /** Implementacion del protocolo IP para linux. */
#include <time.h>           /** Manejo de hora y fecha del sistema */

void * receive_ack( void *ptr );
void process_packet(unsigned char* , int);
unsigned short csum(unsigned short * , int );
char * hostname_to_ip(char * );
int get_local_ip (char *);

struct pseudo_header    /** necesita calculo de checksum */
{
    unsigned int source_address;
    unsigned int dest_address;
    unsigned char placeholder;
    unsigned char protocol;
    unsigned short tcp_length;

    struct tcphdr tcp;
};

struct in_addr dest_ip;

int main(int argc, char *argv[])
{
    /** Crear socket crudo */
    int s = socket (AF_INET, SOCK_RAW , IPPROTO_TCP);
    if(s < 0)
    {
        printf ("Error creando socket. Error de numero : %d . Mensaje de error : %s \n" , errno , strerror(errno));
        exit(0);
    }
    else
    {
        printf("Socket creado.\n");
    }

    /** Datagrama que representa el paquete */
    char datagram[4096];

    /** Cabecera IP */
    struct iphdr *iph = (struct iphdr *) datagram;

    /** Cabecera TCP */
    struct tcphdr *tcph = (struct tcphdr *) (datagram + sizeof (struct ip));

    struct sockaddr_in  dest;
    struct pseudo_header psh;

    char *objetivo = argv[1];

    if(argc < 2)
    {
        printf("Se ejecuta como: ./tcp.out [hostname] \n");
        exit(1);
    }

    if( inet_addr( objetivo ) != -1)
    {
        dest_ip.s_addr = inet_addr( objetivo );
    }
    else
    {
        char *ip = hostname_to_ip(objetivo);
        if(ip != NULL)
        {
            printf("%s resuelto para %s \n" , objetivo , ip);
            /** Convertir nombre de dominio a IP */
            dest_ip.s_addr = inet_addr( hostname_to_ip(objetivo) );
        }
        else
        {
            printf("No se pudo resolver ese hostname : %s" , objetivo);
            exit(1);
        }
    }

    int source_port = 43591;
    char source_ip[20];
    get_local_ip( source_ip );

    printf("IP local %s \n" , source_ip);

    memset (datagram, 0, 4096); /** Cero en el buffer. */

    /** Llenar la cabecera IP */
    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = sizeof (struct ip) + sizeof (struct tcphdr);
    iph->id = htons (54321); /** ID del paquete */
    iph->frag_off = htons(16384);
    iph->ttl = 64;
    iph->protocol = IPPROTO_TCP;
    iph->check = 0;      /** Poner en cero antes de calular el checksum */
    iph->saddr = inet_addr ( source_ip );    /** Falsificar la dirección IP de origen */
    iph->daddr = dest_ip.s_addr;

    iph->check = csum ((unsigned short *) datagram, iph->tot_len >> 1);

    /** Cabecera TCP */
    tcph->source = htons ( source_port );
    tcph->dest = htons (80);
    tcph->seq = htonl(1105024978);
    tcph->ack_seq = 0;
    tcph->doff = sizeof(struct tcphdr) / 4;      /** Tamaño de cabecera TCP*/
    tcph->fin=0;
    tcph->syn=1;
    tcph->rst=0;
    tcph->psh=0;
    tcph->ack=0;
    tcph->urg=0;
    tcph->window = htons ( 14600 );  /**  Maximo tamaño de ventana permitido */
    tcph->check = 0; /** Si establece una suma de comprobación en cero, la pila IP de su kernel debe rellenar la suma de comprobación correcta durante la transmisión */
    tcph->urg_ptr = 0;

    /** IP_HDRINCL Para decirle al kernel que los encabezados están incluidos en el paquete */
    int one = 1;
    const int *val = &one;

    if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0)
    {
        printf ("Error setting IP_HDRINCL. Error number : %d . Error message : %s \n" , errno , strerror(errno));
        exit(0);
    }

    printf("Iniciando scanner...\n");
    char *message1 = "Thread 1";
    int  iret1;
    pthread_t escaner_thread;

    if( pthread_create( &escaner_thread , NULL ,  receive_ack , (void*) message1) < 0)
    {
        printf ("Could not create escaner thread. Error number : %d . Error message : %s \n" , errno , strerror(errno));
        exit(0);
    }

    printf("Enviando paquetes...\n");

    int port;
    dest.sin_family = AF_INET;
    dest.sin_addr.s_addr = dest_ip.s_addr;
    /** Rango de escaneo */
    for(port = 1 ; port <= 100 ; port++)
    {
        tcph->dest = htons ( port );
        tcph->check = 0; /**  el kernel hace el checksum si el valor es cero */

        psh.source_address = inet_addr( source_ip );
        psh.dest_address = dest.sin_addr.s_addr;
        psh.placeholder = 0;
        psh.protocol = IPPROTO_TCP;
        psh.tcp_length = htons( sizeof(struct tcphdr) );

        memcpy(&psh.tcp , tcph , sizeof (struct tcphdr));

        tcph->check = csum( (unsigned short*) &psh , sizeof (struct pseudo_header));

        /** Envia el paquete */
        if ( sendto (s, datagram , sizeof(struct iphdr) + sizeof(struct tcphdr) , 0 , (struct sockaddr *) &dest, sizeof (dest)) < 0)
        {
            printf ("Error Enviando syn packet. Error number : %d . Error message : %s \n" , errno , strerror(errno));
            /*return 0;*/
        }

    }

    pthread_join( escaner_thread , NULL);
    printf("%d" , iret1);


    return 0;
}


int start_escaner()
{
    int sock_raw;

    clock_t t_ini, t_fin;
    t_ini = clock();
    t_fin = clock();

    int saddr_size , data_size;
    struct sockaddr saddr;

    unsigned char *buffer = (unsigned char *)malloc(65536); /** Reservar una cantidad de buffer considerable por si acaso (65536) */

    printf("Recibiendo paquetes...\n");
    fflush(stdout);

    /** Crea un socket crudo */
    sock_raw = socket(AF_INET , SOCK_RAW , IPPROTO_TCP);

    if(sock_raw < 0)
    {
        printf("Socket Error\n");
        fflush(stdout);
        return 1;
    }

    saddr_size = sizeof saddr;

    while( ( (double)(t_fin - t_ini) / CLOCKS_PER_SEC ) * 1000.0 < 0.8 ) /** 0.5 son 5 segundos aprox */
    {
       t_fin = clock();

        /** Recibe paquete */
        data_size = recvfrom(sock_raw , buffer , 65536 , 0 , &saddr , &saddr_size);

        if(data_size <0 )
        {
            printf("Recvfrom error , fallo en obtener paquetes\n");
            fflush(stdout);
            return 1;
        }

        /** Ahora procesa el paquete */
        process_packet(buffer , data_size);
    }

    printf("Escaneo terminado.");
    fflush(stdout);
    return 0;
}



/**
     Funcion de escaneo  recibiendo paquetes y observar respuetas ACK
*/
void * receive_ack( void *ptr )
{
    /** Inicia el escaneo */
    start_escaner();
}

void process_packet(unsigned char* buffer, int size)
{
    /** Obtiene la cabecera TCP del paquete */
    struct iphdr *iph = (struct iphdr*)buffer;
    struct sockaddr_in source,dest;
    unsigned short iphdrlen;

    if(iph->protocol == 6)
    {
        struct iphdr *iph = (struct iphdr *)buffer;
        iphdrlen = iph->ihl*4;

        struct tcphdr *tcph=(struct tcphdr*)(buffer + iphdrlen);

        memset(&source, 0, sizeof(source));
        source.sin_addr.s_addr = iph->saddr;

        memset(&dest, 0, sizeof(dest));
        dest.sin_addr.s_addr = iph->daddr;

        if(tcph->syn == 1 && tcph->ack == 1 && source.sin_addr.s_addr == dest_ip.s_addr )
        {
            printf("Port %d open \n" , ntohs(tcph->source));
            fflush(stdout);
        }
    }
}

/**
 Checksums - IP y TCP
 */
unsigned short csum(unsigned short *ptr,int nbytes)
{
    register long sum;
    unsigned short oddbyte;
    register short answer;

    sum=0;
    while(nbytes>1) {
        sum+=*ptr++;
        nbytes-=2;
    }
    if(nbytes==1) {
        oddbyte=0;
        *((u_char*)&oddbyte)=*(u_char*)ptr;
        sum+=oddbyte;
    }

    sum = (sum>>16)+(sum & 0xffff);
    sum = sum + (sum>>16);
    answer=(short)~sum;

    return(answer);
}

/**
    Obtiene ip de un nombre de dominio
 */
char* hostname_to_ip(char * hostname)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;

    if ( (he = gethostbyname( hostname ) ) == NULL)
    {
        /**  obtiene información del host */
        herror("gethostbyname");
        return NULL;
    }

    addr_list = (struct in_addr **) he->h_addr_list;

    for(i = 0; addr_list[i] != NULL; i++)
    {
        /** Regresa el primero */
        return inet_ntoa(*addr_list[i]) ;
    }

    return NULL;
}

/**
Obtener ip de sistema , como 192.168.0.6 o 192.168.1.2
 */

int get_local_ip ( char * buffer)
{
    int sock = socket ( AF_INET, SOCK_DGRAM, 0);

    const char* kGoogleDnsIp = "8.8.8.8";
    int dns_port = 53;

    struct sockaddr_in serv;

    memset( &serv, 0, sizeof(serv) );
    serv.sin_family = AF_INET;
    serv.sin_addr.s_addr = inet_addr(kGoogleDnsIp);
    serv.sin_port = htons( dns_port );

    int err = connect( sock , (const struct sockaddr*) &serv , sizeof(serv) );

    struct sockaddr_in name;
    socklen_t namelen = sizeof(name);
    err = getsockname(sock, (struct sockaddr*) &name, &namelen);

    const char *p = inet_ntop(AF_INET, &name.sin_addr, buffer, 100);


}
