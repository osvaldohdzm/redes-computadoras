# Escaner ARP

Este programa envia solicitudes ARP para recibir las direcciones de MAC de las direcciones IP en un rango.

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux.

Se debe tener instalado el sistema gestor de base de datos para mysql. Es necesario modificar los siguientes elementos en el programa, con el fin de realizar la conexión a la base de datos mysql.

```
char *server = "localhost";
char *user = "username";
char *password = "xxxxx";
char *database = "xxxxx";
```

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o scanner-arp.out scanner-arp.c -w -lpthread `mysql_config --cflags --libs`

```

O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile scanner-arp

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa]
```

### Ejemplo

```
 sudo ./scanner-arp.out
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
