/**
 * Autor: Osvaldo Hernández Morales
 * Fecha: 31/01/2017, 25/04/2017
 */

#include <stdio.h>              /** Cabecera estandar de entrada y salida. */
#include <string.h>             /** Cabecera estandar con funciones y tipos para manipulación de memoria. */
#include <stdlib.h> 				    /** Cabecera estandar con funciones para gestión de memoria dinámica, control de procesos y otras. */
#include <pthread.h>				    /** Cabecera UNIX para gestión de subprocesos. */
#include <sys/ioctl.h>          /** Cabecera UNIX para controlar o comunicarse con un driver de dispositivo. */
#include <sys/socket.h>         /** Cabecera Linux para el manejo de sockets. */
#include <netpacket/packet.h>		/** Cabecera Linux para el manejo de paquetes en red. */
#include <net/ethernet.h>       /** Definiciones de operaciones de Internet. */
#include <sys/types.h>          /** Cabecera con funciones de búsqueda y ordenamiento de archivos*/
#include <net/if.h>             /**  Funciones para interactuar con el kernel linux y las interfaces de red. */
#include <features.h>           /** Macros estandar para interactuar con otras librerias. */
#include <arpa/inet.h>          /** Cabecera para usar funciones con dircciones ip. */
#include <asm/types.h>          /** Cabecera para manejo de archivos especificos en sistemas POSIX. */
#include <unistd.h>             /**  Cabecera para acceso a API sistema operativo POSIX para versiones UNIX. */
#include <sys/time.h>           /** Funciones para manipular y formatear la fecha y hora alterativa en sistemas UNIX. */

unsigned char Ethertype[2] = {0x08,0x06}; 	/** protocolo . */
unsigned char TramaArpEnviada[42];
unsigned char TramaArpRecibida[42];
unsigned char CodigoEnvio[2] = {0x00,0x01};
unsigned char CodigoRespuesta[2] = {0x00,0x02};
unsigned char IpAuxiliar[4];
unsigned char MacAuxiliar[6];

typedef struct sred
{
	int indice;
	int paquete;
	unsigned char ip[4];
	unsigned char mac[6];
	unsigned char mascara[4];
	unsigned char ipenlace[4];
	unsigned char macenlace[6];
	unsigned char ippu[4];
	struct sred *sig;
}sred;

void parametros(int argc,char **argv,int *ciclo,unsigned char *tuip);
void putip(unsigned char *ip);
void putmac(unsigned char *mac);
void putdatos(sred *EstructuraRed);
sred * datosred(char *interfaz,int paq);
int recibearp(sred *EstructuraRed);
void enviaarp(sred *EstructuraRed);
void estructuraarp(sred *EstructuraRed,unsigned char *tuip);
unsigned char tuip[4],tumac[6];
sred *EstructuraRed=NULL;

/** Esta función asocia los parametros de una estructura de red a una ip dada.
  * Parametros: Argumentos principales del programa en la función principal.
  * Regresa: Nada.
  */
void *esvia(void *args)
{
	estructuraarp(EstructuraRed,tuip);
	return 0;
}

int i; /** Contador usado en funciones de hilos. */

int main(int argc,char *argv[])
{
        int paquete=socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ALL)),ciclo=1,j;
        if(paquete==-1)
                {puts("error al abrir el socket");return 1;}

	if (argc!=5)
	{
	      printf("Uso: %s -I [interface] -i [ip]\n",argv[0]);
	      exit(-1);
    	}

        else
        {
                parametros(argc,argv,&ciclo,tuip);
                EstructuraRed=datosred(argv[2],paquete);
                putdatos(EstructuraRed);
		pthread_t hilo[ciclo];
		if(ciclo>1)
		{
			for(j=0;j<ciclo;j++)
				pthread_create(&hilo[ciclo],NULL,esvia,NULL);
		}
		else
			estructuraarp(EstructuraRed,tuip);
		pthread_join(hilo[ciclo],NULL);
        }
        return 0;
}

void parametros(int argc,char **argv,int *ciclo,unsigned char *tuip)//./a.out wlan0 -i 192.168.1.15
{
        int yapuso=0;
        for(i=0;i<argc;i++)
        {
                if(!memcmp(argv[i],"-i",2)&&yapuso==0)
                {
                        sscanf(argv[i+1],"%d.%d.%d.%d",(int*)&tuip[0],(int*)&tuip[1],(int*)&tuip[2],(int*)&tuip[3]);
                        yapuso=1;
                }
		if(!memcmp(argv[i],"-p",2))
		{
			sscanf(argv[i+1],"%d",ciclo);
		}
         }
        if(yapuso==0) /** 0 es falso y cualquier otro valor !0 verdadero. */
        {
                printf("ERROR\n porfavor proporcione una IP \n");
                exit(-1);
        }
}
void putip(unsigned char *ip)
{
	for(i=0;i<4;i++)
	{
		printf("%d",ip[i]);
		if(i==3)
			continue;
		printf(".");
	}
}
void putmac(unsigned char *mac)
{
	for(i=0;i<6;i++)
        {
                printf("%.2x",mac[i]);
                if(i==5)
                        continue;
                printf(":");
        }
}
void putdatos(sred *EstructuraRed)
{
	printf("\n\t\tpaquete:\t\t%d",EstructuraRed->paquete);
	printf("\n\t\tindice:\t\t\t%d",EstructuraRed->indice);
	printf("\n\t\tIP origen:\t\t");putip(EstructuraRed->ip);
	printf("\n\t\tMAC origen:\t\t");putmac(EstructuraRed->mac);
/**	printf("\n\t\tmascara:\t\t");putip(EstructuraRed->mascara); . */

}
sred * datosred(char *interfaz,int paq)
{
        struct ifreq infointerfaz;
	sred *nvor;
        strcpy(infointerfaz.ifr_name,interfaz);
	nvor=(sred *)malloc(sizeof(sred));
	nvor->paquete=paq;
        if(ioctl(paq,SIOCGIFINDEX,&infointerfaz)==-1)
                {perror("\nerror al obtener indice");exit(-1);}
        nvor->indice=infointerfaz.ifr_ifindex;
        if(ioctl(paq,SIOCGIFHWADDR,&infointerfaz)==-1)
                {perror("\nerro al obtener la mac");}
        else
                {memcpy(nvor->mac+0,infointerfaz.ifr_hwaddr.sa_data,6);}
        if(ioctl(paq,SIOCGIFADDR,&infointerfaz)==-1)
                {perror("\nerro al obener la ip");}
        else
                {memcpy(nvor->ip+0,infointerfaz.ifr_addr.sa_data+2,4);}
        if(ioctl(paq,SIOCGIFNETMASK,&infointerfaz)==-1)
                {perror("\nerror al obtener la mascara de subrred");}
        else
                {memcpy(nvor->mascara,infointerfaz.ifr_netmask.sa_data+2,4);}
        return nvor;
}

int recibearp(sred *EstructuraRed)
{
        struct timeval start, end;
        double time;
	gettimeofday(&start, NULL);
        while(time<2000)
        {
                int tam;
                tam=recvfrom(EstructuraRed->paquete,TramaArpRecibida,48,0,NULL,0);
                if(tam==-1)
                        {perror("\nError al recibir trama");}
                else
                {
                        if(!memcmp(TramaArpRecibida+20,CodigoRespuesta,2)&&!memcmp(TramaArpRecibida+0,EstructuraRed->mac,6)&&!memcmp(TramaArpRecibida+28,IpAuxiliar,4))
                        {
                                memcpy(MacAuxiliar+0,TramaArpRecibida+6,6);
				printf("\n\t\tIP destino:\t\t");putip(IpAuxiliar);
                                printf("\n\t\tMAC destino:\t\t");putmac(MacAuxiliar);printf("\n");
                                return 0;
                        }
                }
                gettimeofday(&end, NULL);
                time =(end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
        }
	return 1;
}

void enviaarp(sred *EstructuraRed)
{
        int tam;
        struct sockaddr_ll nic;
        memset(&nic, 0x00, sizeof(nic));
        nic.sll_family = AF_PACKET;
        nic.sll_protocol = htons(ETH_P_ALL);
        nic.sll_ifindex = EstructuraRed->indice;
        tam = sendto(EstructuraRed->paquete, TramaArpEnviada,48, 0, (struct sockaddr *)&nic, sizeof(nic));
        if(tam == -1)
                perror("\nError al enviar\n\n");
	else
	{
		if(recibearp(EstructuraRed))
			printf("\n respuesta fallida!!! :(");
	}
}

/** Esta función crea la estructura de una trama ARP.
  * Parametros: Información de red en trama a enviar, Apuntador a estructura ip.
  * Regresa: Nada
  *
  * Según el estandar de la estructura de trama ARP se ocupan los bytes necesarios.
 .*/
void estructuraarp(sred *EstructuraRed,unsigned char *tuip)
{
	memcpy(IpAuxiliar,tuip,4);
	memset(TramaArpEnviada+0,0xff,6);
  memcpy(TramaArpEnviada+6,EstructuraRed->mac,6);
  memcpy(TramaArpEnviada+12,Ethertype,2);
	TramaArpEnviada[14]=0x00;  /** harware. */
	TramaArpEnviada[15]=0x01;  /** harware. */
	TramaArpEnviada[16]=0x08;  /** protocolo. */
	TramaArpEnviada[17]=0x00;  /** protocolo. */
  TramaArpEnviada[18]=0x06; /** ldh. */
  TramaArpEnviada[19]=0x04;  /** ldp. */
  memcpy(TramaArpEnviada+20,CodigoEnvio,2);
  memcpy(TramaArpEnviada+22,EstructuraRed->mac,6);
  memcpy(TramaArpEnviada+28,EstructuraRed->ip,4);
	memset(TramaArpEnviada+32,0xff,6);
  memcpy(TramaArpEnviada+38,tuip,4);
	enviaarp(EstructuraRed);
}
