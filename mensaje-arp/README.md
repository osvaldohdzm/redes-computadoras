# Mensaje ARP

Este programa envia una solicitud ARP a una dirección IP dada, y registra la dirección MAC destino en caso de estar en uso esa dirección IP.

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux con permisos de administrador.

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc  mensaje-arp.c -o mensaje-arp.out -Wall -Werror -lpthread

```

O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile mensaje-arp

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa] -I [interface] -i [ip]
```

### Ejemplo

```
sudo ./mensaje-arp.out -I wlp3s0 -i 192.168.1.254
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
