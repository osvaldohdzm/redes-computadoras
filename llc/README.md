# Analizador LLC

Este programa analiza tramas UDP, TCP, ICMP y ARP  capturadas por un programa que emplea la librería winpcap (Wireshark). Para más información sobre los campos de dirección y campo de control en HDLC [ver](http://www.comunidad.escom.ipn.mx/ncortez/rc/HDLC.pdf).

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux. De forma opcional se debe tener instalado el programa Wireshark. En el archivo si se pretende modificar o añadir una trama debe ser en el formato de Wireshark en "File -> Export Packet Dissections -> As C Arrays". Para realizar algunos filtros útiles [ver](https://wiki.wireshark.org/DisplayFilters).

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o llc.out llc.c -w

```

La razón del parametro "-w" es para omitir warnnings remanentes. O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile llc

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa]
```

### Ejemplo

```
 sudo ./llc.out
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
