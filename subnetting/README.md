# Subneteo

Subnetear es la acción de tomar un rango de direcciones IP donde todas las IPs sean locales unas
con otras y dividirlas en diferentes rangos, o subnets, donde las direcciones IPs de un rango serán
remotas de las otras direcciones.

Para más información con referncia CISCO sobre el subnetting [ver](http://www.cisco.com/c/es_mx/support/docs/ip/routing-information-protocol-rip/13788-3.html) y [ver](http://ual.dyndns.org/Biblioteca/Redes/Pdf/Unidad%2006.pdf).

### Prerequisitos

El código puede ser compilado en sistemas operativos Windows, Linux o MAC OS

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o subnetting.out subnetting.c -lm
```

La razón del parametro "-lm" es por el uso del archivo de cabecera "math.h".

```
makefile llc

```

### Ejecución

Para ejecutar el programa se debe

```
./[programa]
```

### Ejemplo

```
 sudo ./subnetting.out
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
