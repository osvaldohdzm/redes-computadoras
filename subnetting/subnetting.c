/**
 * Autor: Osvaldo Hernández Morales
 * Fecha: 31/01/2017, 25/04/2017
 */

#include <stdio.h>          /** Cabecera estandar de entrada y salida. */
#include <stdlib.h>  	      /** Cabecera estandar con funciones para gestión de memoria dinámica, control de procesos y otras. */
#include <string.h>         /** Cabecera estandar con funciones y tipos para manipulación de memoria. */
#include <math.h>           /** Cabecera estandar con funciones y operaciones matemáticas. */

char TipoClase(int oct){
	if ((oct & 128) == 0)
		return 65;
	else if (((oct & 128) == 128) &&((oct & 64) == 0))
		return 66;
	else if (((oct & 128) == 128) &&((oct & 64) == 64) &&((oct & 32) == 0))
		return 67;
	else if (((oct & 128) == 128) &&((oct & 64) == 64) &&((oct & 32) == 32)&&((oct & 16) == 0))
		return 68;
	else if (((oct & 128) == 128) &&((oct & 64) == 64) &&((oct & 32) == 32)&&((oct & 16) == 16))
		return 69;
	else
		return 0;
}

int main(){
	int espacios=0;
	unsigned long int MaximasSubredes=0;
	unsigned long int DirBroad=0;
	unsigned long int MaximosHosts=0;
	unsigned long int i,j;
	char cadena[15];
	int bitsRed=0;
	int bitsSubred=0;
	int tipored=0;
	int contador=0, bandera=0, opcion=0,cantidad=0,numBits;
	char octeto[4];
	unsigned char IP[4]={0,0,0,0};
	unsigned char IP2[4]={0,0,0,0};
	unsigned char MR[4];
	unsigned char IpRed[4];
	unsigned char IpBroad[4];
	unsigned char IPSubredes[4]={0,0,0,0};

	printf("Intruduce una Ip: ");
	scanf("%s",cadena);
	octeto[0]='\0';
	octeto[1]='\0';
	octeto[2]='\0';
	octeto[3]='\0';
	for (i=0;i<strlen(cadena);i++)
	{
		if (cadena[i]=='.')
		{
			octeto[3]='\0';
			IP[bandera]= atoi(octeto);
			octeto[0]='\0';
			octeto[1]='\0';
			octeto[2]='\0';
			bandera++;
			contador=0;
		}
		else
		{
			octeto[contador]=cadena[i];
			contador++;
		}
	}
	IP[bandera]= atoi(octeto);
	i=atoi(cadena);
	if(i>255)
	{
		printf("La IP no es valida\n");
		return 0;
	}
	printf("La IP es: ");
	for (i=0;i<4;i++)
	{
		printf("%d",IP[i]);
		if(i<3)
			printf(".");
	}

	/** Clase. */
	if(TipoClase(IP[0])!=0)
		printf("\nla clase es: %c \n", TipoClase(IP[0]));
	else
		printf("\nDireccion no valida \n");
	/** Mascara de red. */
	MR[0]=0;
	MR[1]=0;
	MR[2]=0;
	MR[3]=0;
	if(TipoClase(IP[0])=='A')
		{
			bitsRed=8;
			MR[0]=255;
			if(IP[1]==0&&IP[2]==0&&IP[3]==0)
				tipored=1;
			else if(IP[1]==255&&IP[2]==255&&IP[3]==255)
				tipored=3;
			else
				tipored=2;
		}
	else if(TipoClase(IP[0])=='B')
		{
			bitsRed=16;
			MR[0]=255;
			MR[1]=255;
			if(IP[2]==0&&IP[3]==0)
				tipored=1;
			else if(IP[2]==255&&IP[3]==255)
				tipored=3;
			else
				tipored=2;
		}
	else if(TipoClase(IP[0])=='C')
		{
			bitsRed=24;
			MR[0]=255;
			MR[1]=255;
			MR[2]=255;
			if(IP[3]==0)
				tipored=1;
			else if(IP[3]==255)
				tipored=3;
			else
				tipored=2;
		}
	printf("La mascara de red es: ");
	for (i=0;i<4;i++)
	{
		printf("%d",MR[i]);
		if(i<3)
			printf(".");
	}
	/** Tipo de IP. */
	if (tipored==1)
		printf("\nEl tipo de IP es de RED");
	else if (tipored==2)
		printf("\nEl tipo de IP es de HOST");
	else if (tipored==3)
		printf("\nEl tipo de IP es de BROADCAST");
	/** IP de Red. */
	printf("\nla IP de Red es: ");
	for (i=0;i<4;i++)
	{
		IpRed[i]=IP[i]&MR[i];
		printf("%d",IpRed[i]);
		if(i<3)
			printf(".");
	}
	/** IP de Broadcast. */
	printf("\nla IP de Broadcast es: ");
	for (i=0;i<4;i++)
	{
		IpBroad[i]=IP[i]|~MR[i];
		printf("%d",IpBroad[i]);
		if(i<3)
			printf(".");
	}
	/** Rango de host. */
	printf("\nEl rango de host es de: ");
	for (i=0;i<4;i++)
	{
		if (i==3)
			printf("%d",IpRed[i]+1);
		else
			printf("%d",IpRed[i]);
		if(i<3)
			printf(".");
	}
	printf(" hasta: ");
	for (i=0;i<4;i++)
	{
		if (i==3)
			printf("%d",IpBroad[i]-1);
		else
			printf("%d",IpBroad[i]);
		if(i<3)
			printf(".");
	}
	/** DG. */
	printf("\nLa puerta de enlace recomendada es: ");
	for (i=0;i<4;i++)
	{
		if (i==3)
			printf("%d",IpBroad[i]-1);
		else
			printf("%d",IpBroad[i]);
		if(i<3)
			printf(".");
	}
	printf("\n");
	/** Parte 2. */
	bandera=0;
	do{
	printf("\n\n�Como desea manejar la red?\n");
	printf("1.-Dividir en subredes\n");
	printf("2.-Dividir en hosts\n");
	printf("3.-Mostrar subredes\n");
	printf("4.-Salir\n");
	printf("Escriba su opcion: ");
	scanf("%d",&opcion);
	printf("\n");
	if(opcion==1)
	{
		MaximasSubredes=pow(2,(30-bitsRed));
		printf("La clase de la red es tipo %c, puedes crear maximo %lu subredes: \n",TipoClase(IP[0]),MaximasSubredes);
		printf("Introduce la cantidad de subredes que deseas: ");
		scanf("%d",&cantidad);
		numBits=ceil(log(cantidad)/log(2));

	}
	else if(opcion==2)
	{
		MaximosHosts=pow(2,(32-bitsRed))-2;
		printf("La clase de la red es tipo %c, puedes crear minimo 4 hosts y maximo %lu hosts: \n",TipoClase(IP[0]),MaximosHosts);
		printf("Introduce la cantidad de hosts que deseas: ");
		scanf("%d",&cantidad);
		numBits=(32-bitsRed)-(ceil(log(cantidad+2)/log(2)));
		if(cantidad<4)
		{
			printf("\nNo se pueden crear menos de 4 hosts\n");
			numBits=0;
		}
	}
	else if (opcion==3)
	{
		i=0;
		printf("\nSe imprimiran todas las subredes: ");
		if ((bitsRed-bitsSubred)==8)
		{
			IPSubredes[0]=IP[0];
		}
			if ((bitsRed-bitsSubred)==16)
		{
			IPSubredes[0]=IP[0];
			IPSubredes[1]=IP[1];
		}
			if ((bitsRed-bitsSubred)==24)
		{
			IPSubredes[0]=IP[0];
			IPSubredes[1]=IP[1];
			IPSubredes[2]=IP[2];
		}
		MaximasSubredes=(IPSubredes[0]<<24)+(IPSubredes[1]<<16)+(IPSubredes[2]<<8)+(IPSubredes[3]);
		printf("\nEl numero es: %lu",MaximasSubredes);
		printf("\nLa ip es: %d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
		IPSubredes[0]=(MaximasSubredes>>24);
		IPSubredes[1]=(MaximasSubredes>>16);
		IPSubredes[2]=(MaximasSubredes>>8);
		IPSubredes[3]=MaximasSubredes;
		printf("\n\nLa ip es: %d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
		MaximosHosts=pow(2,32-bitsRed);
		DirBroad=MaximasSubredes;
		FILE *fp=fopen( "fichero.txt", "w+" );
		fprintf(fp,"%s %d.%d.%d.%d","la ip es:",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
		fprintf(fp,"\nse dividio en: %.0f subredes",pow(2,(bitsSubred)));
		fprintf(fp,"\n\n|No de subred|IP de subred   |Rango de hosts                 |Default gateway|Dir. broadcast |mascara de red |");
		printf("\n\n|No de subred|IP de subred   |Rango de hosts                 |Default gateway|Dir. broadcast |mascara de red |");
		while (i<pow(2,bitsSubred)&&bandera==1)
		{
			contador=0;
			printf("\n|%lu",i);
			fprintf(fp,"\n|%lu",i);
			if(i<10)
				espacios=11;
			else if(i<100)
				espacios=10;
			else if(i<1000)
				espacios=9;
			else if(i<10000)
				espacios=8;
			else if(i<100000)
				espacios=7;
			else if(i<1000000)
				espacios=6;
			else if(i<1000000)
				espacios=5;
			else if(i<10000000)
				espacios=4;
			else
				espacios=3;
			while (espacios>0)
			{
				printf(" ");
				fprintf(fp," ");
				espacios--;
			}
			printf("|");
			fprintf(fp,"|");

			for(j=0;j<4;j++)
			{
				if (IPSubredes[j]<10)
					espacios+=2;
				else if (IPSubredes[j]<100)
					espacios+=1;
			}
			printf("%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			fprintf(fp,"%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			while (espacios>0)
			{
				printf(" ");
				fprintf(fp," ");
				espacios--;
			}
			printf("|");
			fprintf(fp,"|");
			IPSubredes[3]++;
			for(j=0;j<4;j++)
			{
				if (IPSubredes[j]<10)
					espacios+=2;
				else if (IPSubredes[j]<100)
					espacios+=1;
			}
			printf("%d.%d.%d.%d-",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			fprintf(fp,"%d.%d.%d.%d-",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);

			while(contador<(32-bitsRed))
			{
				DirBroad=DirBroad|(1<<contador);
				contador++;
			}

			IPSubredes[0]=(DirBroad>>24);
			IPSubredes[1]=(DirBroad>>16);
			IPSubredes[2]=(DirBroad>>8);
			IPSubredes[3]=DirBroad;
			IPSubredes[3]--;
			for(j=0;j<4;j++)
			{
				if (IPSubredes[j]<10)
					espacios+=2;
				else if (IPSubredes[j]<100)
					espacios+=1;
			}
			fprintf(fp,"%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			printf("%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			while (espacios>0)
			{
				printf(" ");
				fprintf(fp," ");
				espacios--;
			}
			printf("|");
			fprintf(fp,"|");
			for(j=0;j<4;j++)
			{
				if (IPSubredes[j]<10)
					espacios+=2;
				else if (IPSubredes[j]<100)
					espacios+=1;
			}
			fprintf(fp,"%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			printf("%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			while (espacios>0)
			{
				printf(" ");
				fprintf(fp," ");
				espacios--;
			}
			printf("|");
			fprintf(fp,"|");
			IPSubredes[3]++;
			for(j=0;j<4;j++)
			{
				if (IPSubredes[j]<10)
					espacios+=2;
				else if (IPSubredes[j]<100)
					espacios+=1;
			}
			fprintf(fp,"%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			printf("%d.%d.%d.%d",IPSubredes[0],IPSubredes[1],IPSubredes[2],IPSubredes[3]);
			while (espacios>0)
			{
				printf(" ");
				fprintf(fp," ");
				espacios--;
			}
			printf("|");
			fprintf(fp,"|");
			for(j=0;j<4;j++)
			{
				if (MR[j]<10)
					espacios+=2;
				else if (MR[j]<100)
					espacios+=1;
			}
			fprintf(fp,"%d.%d.%d.%d",MR[0],MR[1],MR[2],MR[3]);
			printf("%d.%d.%d.%d",MR[0],MR[1],MR[2],MR[3]);
			while (espacios>0)
			{
				printf(" ");
				fprintf(fp," ");
				espacios--;
			}
			printf("|");
			fprintf(fp,"|");
			MaximasSubredes+=MaximosHosts;
			DirBroad+=MaximosHosts;
			IPSubredes[0]=(MaximasSubredes>>24);
			IPSubredes[1]=(MaximasSubredes>>16);
			IPSubredes[2]=(MaximasSubredes>>8);
			IPSubredes[3]=MaximasSubredes;
			i++;
		}
		fclose ( fp );
		if (bandera==0)
			printf("\nNo hay subredes aun :c");
		numBits=0;
		printf("\n");
	}
	else if (opcion==4)
	{
		printf("\nHas elegido salir\n");
		return 0;
	}
	if(numBits>(30-bitsRed))
			{
				printf("\nLa cantidad de subredes es imposible\n");
			}
		else if(numBits<=(30-bitsRed)||numBits>=0)
			{
				printf("\nEl numero de bits que usara para subredes sera: %d",numBits);
				printf("\nEl numero de bits que usara para host sera: %d",(32-bitsRed)-numBits);
				for(i=0;i<numBits;i++)
				{
					bitsRed++;
					if(MR[0]<255)
					{
						MR[0]=MR[0]>>1;
						MR[0]=MR[0]|128;
					}
					else if(MR[1]<255)
					{
						MR[1]=MR[1]>>1;
						MR[1]=MR[1]|128;
					}
					else if(MR[2]<255)
					{
						MR[2]=MR[2]>>1;
						MR[2]=MR[2]|128;
					}
					else if(MR[3]<255)
					{
						MR[3]=MR[3]>>1;
						MR[3]=MR[3]|128;
					}
				}
				bitsSubred+=numBits;
				printf("\nLa mascara de subred es: %d.%d.%d.%d",MR[0],MR[1],MR[2],MR[3]);
				/** Imprime la lista de las subredes. */
				printf("\nLa cantidad de subredes son %.0f", pow(2,bitsSubred));
				printf("\nLa cantidad de host son %.0f (%.0f menos la ip de broadcast y de red )", (pow(2,(32-bitsRed))-2),(pow(2,(32-bitsRed))));
				printf("\nEl numero de bits de RED son: %d", bitsRed-bitsSubred);
				printf("\nEl numero de bits de SUBRED son: %d", bitsSubred);
				if(bitsRed==30)
				{
					printf("\nYa no se pueden crear mas subredes debido a que solo quedan 4 hosts");
				}
				bandera=1;
			}
	}while (1);
return 0;
}
