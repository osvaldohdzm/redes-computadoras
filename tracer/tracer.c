/**
 * Autor: Osvaldo Hernández Morales
 * Fecha: 31/01/2017, 25/04/2017
 */

#include <stdio.h>              /** Cabecera estandar de entrada y salida. */
#include <stdlib.h>   	        /** Cabecera estandar con funciones para gestión de memoria dinámica, control de procesos y otras. */
#include <string.h>             /** Cabecera estandar con funciones y tipos para manipulación de memoria. */
#include <ctype.h>              /** Funciones que son útiles para probar y asignar caracteres. */
#include <time.h>               /** Funciones para manipular y formatear la fecha y hora. */
#include <sys/time.h>           /** Funciones para manipular y formatear la fecha y hora alterativa en sistemas UNIX. */
#include <sys/types.h>          /** Cabecera con funciones de búsqueda y ordenamiento de archivos*/
#include <sys/socket.h>         /** Cabecera para el manejo de sockets UNIX. */
#include <netinet/in.h>         /** Cabecera que define tipos de datos para protocolos de internet. */
#include <arpa/inet.h>          /** Cabecera para usar funciones con dircciones ip. */
#include <netpacket/packet.h>   /** Cabecera Linux para el manejo de paquetes en red. */
#include <net/ethernet.h>       /** Definiciones de operaciones de Internet. */
#include <net/if.h>             /** Funciones para interactuar con el kernel linux y las interfaces de red. */
#include <sys/ioctl.h>          /** Cabecera UNIX para controlar o comunicarse con un driver de dispositivo. */
#include <unistd.h>             /**  Cabecera para acceso a API sistema operativo POSIX para versiones UNIX. */
#include <fcntl.h>              /** Define argumentos usados por fcntl() y open().*/
#include <netdb.h>              /** Definiciones de operaciones de base de datos de red. */

extern int h_errno;

unsigned char MACorigen[6]={0x00},MACdestino[6]={0x00},Masc[4]; /** Datos Generales */
unsigned char IPorigen[4]={0x00},IPdestino[4]={0x00},IPgateway[4]={0x00};
unsigned char MACbro[6]={0xff,0xff,0xff,0xff,0xff,0xff}; /** MAC broadcast */
unsigned char MACempty[6]={0x00,0x00,0x00,0x00,0x00,0x00}; /** MAC vacia */
unsigned char etherARP[2]={0x08,0x06}; /** Reconoce el protocolo ARP */
unsigned char etherPing[2]={0x08,0x00}; /** Reconoce el protocolo Ping */
unsigned char msn[22]={0x4d,0x6f,0x69,0x73,0x65,0x73,0x00,0x4d,0x6f,
0x72,0x61,0x6c,0x65,0x73,0x00,0x41,0x6c,0x6d,0x61,0x72,0x61,0x7a }; /**  Contenido */
unsigned char longDatGram[2]={0x00,0x32}; /** (ICMP=8)+(EncIP=20)+Contenido(22) = 50 */
unsigned char id_ip[2]={0x00,0x00}; /** Identificador IP */
unsigned char check_sum_ip[2]={0x00,0x00}; /**  Check sum ip */
unsigned char check_sum_icmp[2]={0x00,0x00}; /** Check sum icmp */
unsigned char id_icmp[2]={0x00,0x00}; /** Identificador icmp */
unsigned char no_sec[2]={0x00,0x00}; /** numero de secuencia */

unsigned char tipoHardware[2]={0x00,0x01}; /** Ethernet */
unsigned char tipoProtocolo[2]={0x08,0x00}; /** Protocolo ARP */
unsigned char lonDirHW=0x06; /** Longitud de direccion Hardware */
unsigned char lonDirIP=0x04; /** Longitud de direccion IP */
unsigned char opcodEnv[2]={0x00,0x01}; /** Solicitud ARP */
unsigned char opcodRes[2]={0x00,0x02}; /** Respuesta ARP */
unsigned char echo_request[1]={0x08}; /** Solicitud de ECO */
unsigned char echo_response[1]={0x00}; /** Respuesta de ECO */
unsigned char time_expired[1]={0x0b}; /** Tiempo excedido */
unsigned char ttl_expired[1]={0x00}; /** TTL expiro */
unsigned char ip_router[4]={0x00,0x00,0x00,0x00};

unsigned char envTrama[1514];
unsigned char recTrama[1514];

int id0,id1,id00,id11,secuence,count=0;
double time_total,min=4000,max=0,avg=0;

/** 	FUNCIONES	*/
int capturaIP(unsigned char ip[4],char *cadena);
int obtenerDatos(int ds);
void estructuraTramaARP(unsigned char *trama,int gateway);
void enviaTrama(int ds,char *paq,int index);
int recibeTramaARP(int ds,unsigned char *trama);
void estructuraTramaPing(unsigned char *trama);
void checksum(int limi, int lims,unsigned char check[2]);
void enviaTramaPing(int ds,char *paq,int index);
int recibeTramaPing(int ds,unsigned char *trama);
void imprimeTrama(unsigned char *paq,int tam);
void imprime_dir(unsigned char *dat,int tam);

int main(int argc,char *argv[]){
struct hostent *h;
int packet_socket=0,indice,por_less,i,ok=0;
char *cadena,*gateway;
cadena=(char *)malloc(sizeof(char)*20);
gateway=(char *)malloc(sizeof(char)*20);
srand(time(NULL)); /** Tiempo random */
/** Ejemplo de ejecución: ./ping 192.168.1.0 */
if(argc>=2){
	cadena=argv[1];
	if( isdigit(cadena[0]) == 0){
		if((h=gethostbyname(argv[1]))==NULL){ /** Obteniendo la info del Host... */
			herror("gethostbyname");
			return 1;
		}
		printf("Host name: %s \n",h->h_name);
		cadena = inet_ntoa(*((struct in_addr*)h->h_addr));
		printf("IP Address: %s \n",cadena);
		capturaIP(IPdestino,cadena);
	}else{
		if( capturaIP(IPdestino,cadena) == 0){
			puts("Dirección IP erronea!");
			return 1;
		}
	}
}else{
	puts("Se ejecuta como: ./tracer www.google.com");
	return 1;
}
packet_socket = socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ALL));
fcntl(packet_socket,F_SETFL,O_NONBLOCK); /** Socket no bloqueante! */
		/** Af_packet -> dominio */
		/** sock_raw -> tipo */
		/** htons(ETH_P_ALL) -> protocolo */
	if(packet_socket==-1){
		puts("Error al crear el socket");
		return 1;
	}
	else {
		puts("Exito al crear el socket");
		indice=obtenerDatos(packet_socket);
		/**  Comprobando si la ip esta en la misma red. */
		for(i=0;i<3;i++) /** Con los primeros 3 octetos */
			if(IPdestino[i]==IPorigen[i])ok++;

		if(ok == 3){ /** Enviar trama ARP con la dir. IP Destino. */
			estructuraTramaARP(envTrama,0); /** Trama para IPdestino */
			enviaTrama(packet_socket,envTrama,indice);
			if( recibeTramaARP(packet_socket,recTrama) != 1 ){
				puts("Error al capturar dir. mac destino");
				return 1;
			}
		}else{ /**  Enviar trama ARP con la dir. IP Gateway. */
			system("ip route show default | grep default | awk {'print $3'} > gateway.txt");
			FILE *archivo;
			archivo = fopen("gateway.txt","r");
			if(archivo!=NULL){
				while( !feof(archivo) ){
					fgets(gateway,20,archivo);
				}
			capturaIP(IPgateway,gateway);
			estructuraTramaARP(envTrama,1); /** Trama para Gateway */
			enviaTrama(packet_socket,envTrama,indice);
			if( recibeTramaARP(packet_socket,recTrama)!=1 ){
				puts("Error al capturar dir. mac destino");
				return 1;
			}
			remove("gateway.txt");
			fclose(archivo);
			}else{ puts("Imposible recuperar Gateway!"); return 1; }
		}
		/** ----------/ Estructurando el TRACERT /----------*/
		printf("\nTraza a la dirección %s [%s]\n",argv[1],cadena);
		printf("Sobre un máximo de 30 saltos: \n\n");
		/** Hacemos 30 un máx de pings */
		while(count<31){
			/** 2° Identificador ICMP constante*/
			id00 = rand() % 255;
			id11 = rand() % 255;
			id_icmp[0]=id00;
			id_icmp[1]=id11;
			estructuraTramaPing(envTrama);
			envTrama[22]=count++;

			for(secuence=0;secuence<3;secuence++){
				/** 1 Identificador IP. */
				id0 = rand() % 255;
				id1 = rand() % 255;
				id_ip[0]=id0;
				id_ip[1]=id1;
				memcpy(envTrama+18,id_ip,2);
				 /**  Numero de Secuencia   . */
					/** Secuencia */
				no_sec[0]=0x00;
				no_sec[1]=secuence;
				memcpy(envTrama+40,no_sec,2);
					/** Check-sums IP */
				checksum(14,34,check_sum_ip);
				memcpy(envTrama+24,check_sum_ip,2);
					/** Check-sum ICMP */
				checksum(34,64,check_sum_icmp);
				memcpy(envTrama+36,check_sum_icmp,2);
					/** Envio y Recibo de tramas */
				enviaTramaPing(packet_socket,envTrama,indice);
				/** imprimeTrama(envTrama,58);puts(""); */
					/** Recibe trama ICMP */
				ok=recibeTramaPing(packet_socket,recTrama);
				/** Limpiamos los Check-Sum */
				check_sum_ip[0]=0x00; check_sum_ip[1]=0x00;
				check_sum_icmp[0]=0x00; check_sum_icmp[1]=0x00;
				memcpy(envTrama+24,check_sum_ip,2);
				memcpy(envTrama+36,check_sum_icmp,2);
	 		}if(ok==1)break;ip_router[0]=0;
 		}
 		if(ok!=1)printf("Mas de 30 saltos desafortunadamente :(\n");
	}
close(packet_socket);
puts("");puts("");
return 0;
}

/** Camptua la IP destino que se ejecuto*/
int capturaIP(unsigned char ip[4],char *cadena){
int tam,i,j=0,k=0,num;
char *cadaux;
cadaux=(char*)malloc(sizeof(char)*4);
tam = strlen(cadena);
	for(i=0;i<=tam;i++){
		if(isdigit(cadena[i])){
			cadaux[j]=cadena[i];
			j++;
		}else{
			num=atoi(cadaux);
			if(num<255){
				ip[k]=num;
				for(j=0;j<4;j++)
					cadaux[j]='\0';
				j=0;k++;
			}else return 0;
		}
	}
return 1;
}

int obtenerDatos(int ds){
char nombre[10];
int i,in;
struct ifreq nic;
printf("Nombre de la interfaz: ");
fgets(nombre,10,stdin);
nombre[strlen(nombre)-1]='\0';

/** Se coloca el nombre de la interfaz */
strcpy(nic.ifr_name,nombre);

/** Se obtiene el indice de la interfaz */
if(ioctl(ds,SIOCGIFINDEX,&nic)==-1)
	perror("Error al obtener indice");
else printf("Index: %d\n",nic.ifr_ifindex);
in = nic.ifr_ifindex;

/** Se obtiene la direccion MAC del dispositivo */
if(ioctl(ds,SIOCGIFHWADDR,&nic)==-1)
	perror("Imposible recuperar dir.mac");
else{
	memcpy(MACorigen+0,nic.ifr_hwaddr.sa_data,6);
	printf("DirecciónHW: ");
	for(i=0;i<6;i++){
		printf("%02x",MACorigen[i]);
		if(i!=5)printf(":");
	}puts("");
}
/** Se obtiene la direccion IP del dispositivo */
if(ioctl(ds,SIOCGIFADDR,&nic)==-1)
	perror("Imposible recuperar dir.ip");
else{
	printf("Direc. inet: ");
	memcpy(IPorigen+0,nic.ifr_addr.sa_data+2,4);
	for(i=0;i<4;i++){
		printf("%d",IPorigen[i]);
		if(i!=3)printf(".");
	}puts("");
}
/** Se obtiene la mascara de subred */
if(ioctl(ds,SIOCGIFNETMASK,&nic)==-1)
	perror("Imposible recuperar Másc");
else{
	printf("Másc: ");
	memcpy(Masc,nic.ifr_netmask.sa_data+2,4);
	for(i=0;i<4;i++){
		printf("%d",Masc[i]);
		if(i!=3)printf(".");
	}puts("");
}
return in;
}

void estructuraTramaARP(unsigned char *trama,int gateway){
memcpy(trama+0,MACbro,6);
memcpy(trama+6,MACorigen,6);
memcpy(trama+12,etherARP,2);
memcpy(trama+14,tipoHardware,2);
memcpy(trama+16,tipoProtocolo,2);
trama[18]=lonDirHW;
trama[19]=lonDirIP;
memcpy(trama+20,opcodEnv,2);
memcpy(trama+22,MACorigen,6);
memcpy(trama+28,IPorigen,4);
memcpy(trama+32,MACempty,6);
if(gateway==0){memcpy(trama+38,IPdestino,4);}
else memcpy(trama+38,IPgateway,4);
}

void enviaTrama(int ds,char *paq,int index){
int tam;
struct sockaddr_ll nic;
memset(&nic,0x00,sizeof(nic));
nic.sll_family = AF_PACKET;
nic.sll_protocol = htons(ETH_P_ALL);
nic.sll_ifindex = index;
tam = sendto(ds,paq,60,0,(struct sockaddr *)&nic,sizeof(nic));
if(tam==-1) puts("Error al enviar");
}

int recibeTramaARP(int ds,unsigned char *trama){
int tam,i;
double tiempo;
struct timeval start,end;
	gettimeofday(&start,NULL);
	while(tiempo<5000){ /** Medido en Milisegundos */
	tam = recvfrom(ds,trama,48,0,NULL,0); /** hacerla funcion no bloqueante */
		if( memcmp(trama+0,MACorigen,6)==0 && memcmp(trama+20,opcodRes,2)==0
		&& memcmp(trama+12,etherARP,2)==0 && memcmp(trama+38,IPorigen,4)==0
		/** && memcmp(trama+28,IPdestino,4)==0 */
		){
			memcpy(MACdestino,trama+6,6);
			return 1;
		}
		gettimeofday(&end, NULL);
      tiempo = (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
	}
return 0;
}

void estructuraTramaPing(unsigned char *trama){
memcpy(trama+0,MACdestino,6);		/** Mac destino */
memcpy(trama+6,MACorigen,6);		/** Mac origen */
memcpy(trama+12,etherPing,2);		/** Ethertype del Ping */
trama[14]=0x45;				/** Version: IPv4 | long.Enc.IP: 5(4) = 20 */
trama[15]=0x00;				/** TypeOfService(TOS0) */
memcpy(trama+16,longDatGram,2);	/** longDatGram = (ICMP=8)+(EncIP=20)+Contenido(22) = 50 */
memcpy(trama+18,id_ip,2);		/** Identificador IP */
trama[20]=0x00;trama[21]=0x00;	/** Banderas y Dezplazamiento */
trama[22]=0x80;				/** TimeToLive TTL = 128 saltos */
trama[23]=0x01;				/** Protocolo ICMP */
memcpy(trama+24,check_sum_ip,2);	/** Check-Sum del Encabezado MAC */
memcpy(trama+26,IPorigen,4);		/** IP origen */
memcpy(trama+30,IPdestino,4);		/** IP destino */
memcpy(trama+34,echo_request,1);	/** Solicitud de ECO */
trama[35]=0x00;				/** Codigo */
memcpy(trama+36,check_sum_icmp,2); 	/** Check-Sum del ICMP */
memcpy(trama+38,id_icmp,2);		/** Identificador ICMP */
memcpy(trama+40,no_sec,2); 		/** Numero de Secuencia */
memcpy(trama+42,msn,22);		/** Mensaje = Moises Morales Almaraz */
}
/**
	NOTA: Si se cambia el mensaje se tiene que cambiar:
	Rango del Check-sum ICMP
	La longitud del datagrama (longDatGram)
	El tamaño de la trama de la función EnviaTrama
*/

void checksum(int limi, int lims,unsigned char check[2])
{
	int i;
	unsigned int suma=0;
	unsigned short aux;
	for(i=limi;i<lims;i+=2)
	{
		aux=envTrama[i];
		aux=(aux<<8)+envTrama[i+1];
		suma=suma+aux;
	}
	suma=(suma&0x0000FFFF)+(suma>>16);
	check[1]=(unsigned char)~(suma);
    check[0]=(unsigned char)~(suma>>8);
}

void enviaTramaPing(int ds,char *paq,int index){
int tam;
struct sockaddr_ll nic;
memset(&nic,0x00,sizeof(nic));
nic.sll_family = AF_PACKET;
nic.sll_protocol = htons(ETH_P_ALL);
nic.sll_ifindex = index;
tam = sendto(ds,paq,64,0,(struct sockaddr *)&nic,sizeof(nic));
if(tam==-1) puts("Error al enviar");
}

int recibeTramaPing(int ds,unsigned char *trama){
int tam,i,j;
double tiempo=0,tiempo2=0;
struct timeval start,end;
	gettimeofday(&start,NULL);
	while(tiempo<1000){ /** Medido en Milisegundos */
		tam=recvfrom(ds,trama,1514,0,NULL,0);
		if( tam != -1){
			if(memcmp(trama+0,MACorigen,6)==0 && memcmp(trama+12,etherPing,2)==0)
			{
				if( memcmp(trama+34,time_expired,1)==0 && memcmp(trama+35,ttl_expired,1)==0 && memcmp(trama+46,id_ip,2)==0 ){
					gettimeofday(&end, NULL);
					if(secuence==0)printf("%d.\t",count);
					tiempo2 = (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
					printf("%.2lf\t\t",tiempo2);
					if(ip_router[0]==0)
						for(i=26,j=0;i<30;i++,j++){
								ip_router[j]= trama[i];
						}
					if(secuence==2 && ip_router[0]!=0){
						imprime_dir(ip_router,4);
						puts("");
					}
					return 0;
				}
				if( memcmp(trama+34,echo_response,1)==0 && memcmp(trama+38,id_icmp,2)==0 ){
					gettimeofday(&end, NULL);
					if(secuence==0)printf("%d.\t",count);
					tiempo2 = (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
					printf("%.2lf\t\t",tiempo2);
					if(ip_router[0]==0)
						for(i=26,j=0;i<30;i++,j++){
								ip_router[j]= trama[i];
						}
					if(secuence==2 && ip_router[0]!=0){
						imprime_dir(ip_router,4);
						puts("");
					}
					return 1;
				}
			}
		} gettimeofday(&end, NULL);
	tiempo = (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
	}
if(secuence==0) printf("%d.\t*\t\t",count);
else if(secuence==1) printf("*\t\t");
else { 	printf("*\t\t");
		if(ip_router[0]!=0)
		imprime_dir(ip_router,4);
		puts("");
	}
return 2;
}

void imprimeTrama(unsigned char *paq,int tam){
int i;
	for(i=0;i<tam;i++){
		if(i%16==0)
			printf("\n");
		printf("%.2x ",paq[i]);
	}
}

void imprime_dir(unsigned char *dat,int tam){
int i=0;
	if(tam == 4){
		for(i=0;i<tam;i++){
			printf("%d",dat[i]);
			if(i!=3)printf(".");
		}
	}
	if(tam == 6){
		for(i=0;i<6;i++){
			printf("%.02x",dat[i]);
			if(i!=5)printf(":");
		}puts("");
	}
}
