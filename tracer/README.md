# Tracer

La utilidad de diagnóstico TRACER determina la ruta a un destino mediante el envío de paquetes de eco de Protocolo de mensajes de control de Internet (ICMP) al destino. En estos paquetes, TRACERT usa valores de período de vida (TTL) IP variables. Dado que los enrutadores de la ruta deben disminuir el TTL del paquete como mínimo una unidad antes de reenviar el paquete, el TTL es, en realidad, un contador de saltos. Cuando el TTL de un paquete alcanza el valor cero (0), el enrutador devuelve al equipo de origen un mensaje ICMP de "Tiempo agotado".


TRACERT envía el primer paquete de eco con un TTL de 1 y aumenta el TTL en 1 en cada transmisión posterior, hasta que el destino responde o hasta que se alcanza el TTL máximo. Los mensajes ICMP "Tiempo agotado" que devuelven los enrutadores intermedios muestran la ruta. Observe, sin embargo, que algunos enrutadores colocan paquetes que han agotado el TTL sin avisar y que estos paquetes son invisibles para TRACERT.

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux.

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o tracer.out tracer.c

```

O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile llc

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa] [hostname]
```

### Ejemplo

```
 sudo ./tracer.out www.google.com
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Juan Jesus Alcaraz** - *Profesor*
* **Osvaldo Hernández Morales** - *Estudiante*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
