/**
 * Autor: Osvaldo Hernández Morales
 * Fecha: 31/01/2017, 25/04/2017
. */

#include <stdio.h>          /** Cabecera estandar de entrada y salida. */
#include <netdb.h>   		    /** Definiciones de operaciones de base de datos de red. */
#include <sys/types.h>      /** Cabecera con funciones de búsqueda y ordenamiento de archivos*/
#include <sys/socket.h>     /** Cabecera para el manejo de sockets UNIX. */
#include <netinet/in.h>     /** Cabecera que define tipos de datos para protocolos de internet. */
#include <stdlib.h>         /** Cabecera para usar la función exit. */
#include <arpa/inet.h>      /** Cabecera para usar funciones con dircciones ip. */

int main(int argc, char *argv[])
{
   struct hostent *he;    /** Este tipo de datos se utiliza para representar una entrada en la base de datos de hosts. */

   if (argc!=2)
   {
      printf("Uso: %s [dominio];\n",argv[0]);    /** Verificar parametros. */
      exit(-1);
   }

   if ((he=gethostbyname(argv[1]))==NULL)    /** Devuelve una estructura de tipo hostent para el nombre dado. */
   {
      printf("error de gethostbyname()\n");
      exit(-1);
   }

   printf("Nombre del host: %s\n",he->h_name);   /** Muestra el nombre del nodo. */
   printf("Dirección IP: %s\n",inet_ntoa(*((struct in_addr *)he->h_addr)));    /** Muestra la dirección IP. */
}
