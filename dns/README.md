# DNS

Este es un programa simple, que obtiene la dirección ip de un dominio, enviando una solicitud dns.

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux.

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o dns.out dns.c

```

O bien, usar el archivo makefile en la carpeta del código fuente.

```
makefile dns

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa] [dominio]
```

### Ejemplo

```
 sudo ./dns.out google.com
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
