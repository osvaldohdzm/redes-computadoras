<p align="center">
  <img width="300" height="300" src="https://pbs.twimg.com/profile_images/1423089146/escom_400x400.png">
  <img width="230" height="300" src="http://www.cectijuana.ipn.mx/v2/images/logipn.png">
</p>

---

# Redes de computadoras

Este proyecto es un conjunto de programas con fines académicos para aprender el funcionamiento de sockets crudos y funciones para manejo de protocolos en red.

## Inicio

Una red de ordenadores, también llamada red de comunicaciones de datos o red informática, es un conjunto de equipos informáticos y software conectados entre sí por medio de dispositivos físicos que envían y reciben impulsos eléctricos, ondas electromagnéticas o cualquier otro medio para el transporte de datos, con la finalidad de compartir información, recursos y ofrecer servicios.

Como en todo proceso de comunicación, se requiere de un emisor, un mensaje, un medio y un receptor. La finalidad principal para la creación de una red de ordenadores es compartir los recursos y la información en la distancia, asegurar la confiabilidad y la disponibilidad de la información, aumentar la velocidad de transmisión de los datos y reducir el costo. Un ejemplo es Internet, el cual es una gran red de millones de ordenadores ubicados en distintos puntos del planeta interconectados básicamente para compartir información y recursos.

### Prerequisitos

La mayor parte del código dedicado a los protocolos debe ser compilado en sistemas Linux o MAC OS, con sus respectivas equivalencias en librerias.

En Windows 7, Windows Vista, Windows XP con Service Pack 2 (SP2) y Windows XP con Service Pack 3 (SP3), la capacidad de enviar tráfico a través de sockets sin procesar se ha restringido de varias maneras:

- Los datos TCP no se pueden enviar a través de sockets sin procesar.
- Los datagramas UDP con una dirección de origen no válida no se pueden enviar a través de sockets sin procesar. La dirección de origen IP para cualquier datagrama UDP saliente debe existir en una interfaz de red o el datagrama se ha eliminado. Este cambio se hizo para limitar la capacidad del código malicioso para crear ataques de denegación de servicio distribuidos y limita la capacidad de enviar paquetes falsificados (paquetes TCP / IP con una dirección IP de origen falsificada).
- No se permite una llamada a la función de enlace con un socket raw para el protocolo IPPROTO_TCP, con un socket raw se permite para otros protocolos (IPPROTO_IP, IPPROTO_UDP o IPPROTO_SCTP, por ejemplo).

En windows por ejemplo, la librería equivalente es:

```
#include "winsock2.h"
```

Como alternativa se puede crear sockets con la librería winpcap.

## Autores

* **Juan Jesus Alcaraz** - *Profesor*
* **Osvaldo Hernández Morales** - *Estudiante*
