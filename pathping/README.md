# Pathping

Este programa proporciona información acerca de la latencia de red y pérdida de la red en saltos intermedios entre un origen y destino. Pathping envía varios mensajes de solicitud de eco a cada enrutador entre un origen y destino durante un período de tiempo y, a continuación, calcula los resultados en función de los paquetes devueltos desde cada enrutador.

Para más información de las funciones usadas en este programa se sugiere usar:

```
man 3 [funcion]

```
### Prerequisitos

El código debe ser compilado y ejecutado en sistemas operativos linux con permisos de administrador.

### Compilación

La compilación de archivos se debe hacer de la siguiente manera:

```
gcc -o pathping.out pathping.c

```

O bien, se puede usar el archivo makefile en la carpeta del código fuente.

```
makefile pathping

```

### Ejecución

Para ejecutar el programa se debe

```
sudo ./[programa] [hostname]
```

### Ejemplo

```
sudo ./pathping.out google.com
```

### Contribuciones

Librerias estandar de LINUX (Ubuntu).

### Autores

* **Osvaldo Hernández Morales** - *Estudiante*
* **Juan Jesus Alcaraz** - *Profesor*

### Licencia

Este software es de uso libre para los estudiantes de la Escuela Superior de Cómputo.
